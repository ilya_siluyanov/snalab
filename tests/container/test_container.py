import pytest
import httpx
import os
import time
from datetime import datetime
import pytest_asyncio


@pytest.fixture
def server_socket() -> str:
    return os.getenv("SERVER_SOCKET", "localhost:9876")


@pytest.fixture(scope="function")
async def wait_for_start(server_socket: str):
    async with httpx.AsyncClient() as client:
        retries = 5
        timeout = 1
        curr = 0
        while curr < retries:
            try:
                res = await client.get(f'http://{server_socket}/health')
                res.raise_for_status()
            except Exception:
                curr += 1
                time.sleep(timeout)
                continue
            else:
                return
    raise Exception(f"Sensor {server_socket} did not start")


async def test_echo_endpoint(server_socket: str, wait_for_start):
    async with httpx.AsyncClient() as client:
        res = await client.get(f'http://{server_socket}')
        assert res.status_code == 200
        delta = datetime.utcnow() - datetime.fromisoformat(res.json()["server_time"])
        assert delta.total_seconds() < 10, "Too large difference in time"


