FROM python:3.10.4-slim-buster

RUN pip install -U pip
COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

COPY app /app
COPY docker-entrypoint.sh /docker-entrypoint.sh

COPY tests /tests
COPY requirements.test.txt ./
COPY test.sh ./

ENTRYPOINT ["bash", "/docker-entrypoint.sh"]
