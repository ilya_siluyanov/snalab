# sna-open-source

```bash
docker build -t my_server .
docker run -p <HOST_PORT>:<INTERNAL_PORT> -ti --rm my_server <INTERNAL_PORT:-80>
```

OR u can use `ilyasiluyanov/my_server:latest` image from Docker Hub: [link](https://hub.docker.com/layers/ilyasiluyanov/my_server/latest/images/sha256-9a959b5862e4ba0de3a1cfb49c6830490685357bc38f1cb4cfce06227abcd1e8?context=repo)
