from fastapi import FastAPI
from datetime import datetime

app = FastAPI()


@app.get("/")
async def get_current_time():
    return {"server_time": datetime.utcnow(), "tz": "UTC"}


@app.get("/health")
async def healthcheck():
    return {"status": "OK"}
